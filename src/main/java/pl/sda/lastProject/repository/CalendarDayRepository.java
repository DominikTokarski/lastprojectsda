package pl.sda.lastProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.lastProject.model.CalendarDay;

import java.time.Month;
import java.util.Optional;

@Repository
public interface CalendarDayRepository extends JpaRepository<CalendarDay,Long> {

    Optional<CalendarDay> findByDayOfMonthAndMonthAndYear(int dayOfMonth, Month month, int year);
}
