package pl.sda.lastProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.lastProject.model.AppUser;
import pl.sda.lastProject.model.CalendarEvent;
import pl.sda.lastProject.model.dto.UpdateUserDto;
import pl.sda.lastProject.service.AppUserService;
import pl.sda.lastProject.service.LoginService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/user")
public class AppUserViewController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private LoginService loginService;


    @GetMapping("/profile")
    public String viewProfile(Model model){
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (!appUserOptional.isPresent()){
            return "redirect:/";
        }
        AppUser appUser = appUserOptional.get();
        List<CalendarEvent> list = appUser.getCalendarEvents();
        model.addAttribute("logged",appUser);
        model.addAttribute("user", appUser);
        model.addAttribute("events_list",list);
        return "user/profile";
    }

    @GetMapping("/profile/{id}")
    public String viewProfile(Model model, @PathVariable(name = "id")Long id){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        Optional<AppUser> appUserOptional = appUserService.findUserById(id);
        if (!appUserOptional.isPresent()){
            return "redirect:/";
        }
        AppUser user = optionalAppUser.get();
        AppUser appUser = appUserOptional.get();
        List<CalendarEvent> list = appUser.getCalendarEvents();
        model.addAttribute("logged",user);
        model.addAttribute("user", appUser);
        model.addAttribute("events_list",list);
        return "user/profile";
    }


    @GetMapping("/all")
    public String getAll(Model model){
        List<AppUser> list = appUserService.getAll();
        model.addAttribute("user_list",list);
        return "user/list";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        appUserService.delete(id);
        return "user/list";
    }

    @GetMapping("/modify/{id}")
    public String update(Model model, @PathVariable(name = "id") Long id) {
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (appUserOptional.get().isAdmin()) {
            Optional<AppUser> optionalAppUser = appUserService.findUserById(id);
            if (optionalAppUser.isPresent()) {
                AppUser appUser  = optionalAppUser.get();
                UpdateUserDto dto = UpdateUserDto.prepareToUpdate(appUser);
                model.addAttribute("modify_user", dto);
                return "user/modify";
            }
            return "user/list";
        }else if (appUserOptional.get().getId() == id) {
            AppUser appUser = appUserOptional.get();
            UpdateUserDto dto = UpdateUserDto.prepareToUpdate(appUser);
            model.addAttribute("modify_user", dto);
            return "user/modify";
        }
        return "user/list";
    }

    @PostMapping("/modify")
    public String update(UpdateUserDto dto){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()){
            appUserService.update(dto);
            return "redirect:/user/list";
        }
        return "redirect:/login";
    }

    @GetMapping("/addFriend/{id}")
    public String addFriend(@PathVariable(name = "id")Long id){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()){
            Optional<AppUser> appUserOptional = appUserService.findUserById(id);
            if (appUserOptional.isPresent()) {
                AppUser appUser = optionalAppUser.get();
                AppUser friend = appUserOptional.get();
                if (appUser.getFriends().contains(friend)){
                    return "view/user/all";
                }
                appUser.getFriends().add(friend);
                appUserService.updateFriends(appUser);
                return "view/user/friendsList";
            }
            return "view/user/all";
        }
        return "redirect:/login";
    }

    @GetMapping("/friendsList")
    public String getFriendsList(Model model){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();
            List < AppUser > friendsList = appUser.getFriends();
            model.addAttribute("friendsList", friendsList);
            return "user/friendsList";
        }
        return "/login";
    }

    @PostMapping("/deleteFriend/{id}")
    public String deleteFriend(@PathVariable(name = "id")Long id){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();
            Optional<AppUser> appUserOptional = appUserService.findUserById(id);
            if (appUserOptional.isPresent()) {
                AppUser friend = optionalAppUser.get();
                appUser.getFriends().remove(friend);
                appUserService.updateFriends(appUser);
            }
            return "view/user/friendsList";
        }
        return "/login";
    }


}
