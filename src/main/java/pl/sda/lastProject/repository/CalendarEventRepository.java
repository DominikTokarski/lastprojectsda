package pl.sda.lastProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.lastProject.model.CalendarEvent;

@Repository
public interface CalendarEventRepository extends JpaRepository<CalendarEvent,Long> {

}
