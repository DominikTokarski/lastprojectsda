package pl.sda.lastProject.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class CalendarDay {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private DayOfWeek dayOfWeek;
    private Integer dayOfMonth;
    private Month month;
    private Integer year;
    private String holidays;


    @OneToMany(fetch = FetchType.EAGER)
    private List<CalendarEvent> calendarEvents = new ArrayList<>();
}
