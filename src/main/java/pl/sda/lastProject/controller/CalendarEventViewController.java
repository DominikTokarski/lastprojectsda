package pl.sda.lastProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.lastProject.model.AppUser;
import pl.sda.lastProject.model.CalendarEvent;
import pl.sda.lastProject.model.dto.AddCalendarEventDto;
import pl.sda.lastProject.model.dto.UpdateCalendarEventDto;
import pl.sda.lastProject.service.AppUserService;
import pl.sda.lastProject.service.CalendarEventService;
import pl.sda.lastProject.service.LoginService;
import pl.sda.lastProject.service.MailingService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/event")
public class CalendarEventViewController {


    @Autowired
    private AppUserService appUserService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private CalendarEventService calendarEventService;

    @Autowired
    private MailingService mailingService;


    @GetMapping("/delete/{id}/{eventId}")
    public String delete(@PathVariable(name = "id") Long id,@PathVariable(name = "eventId") Long eventId) {
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        Optional<CalendarEvent> optionalCalendarEvent = calendarEventService.findById(eventId);
        if (appUserOptional.get().isAdmin()) {
            Optional<AppUser> optionalAppUser = appUserService.findUserById(id);
            if (optionalAppUser.isPresent()) {
                AppUser appUser = optionalAppUser.get();
                CalendarEvent calendarEvent = optionalCalendarEvent.get();
                appUser.getCalendarEvents().remove(calendarEvent);
            }
            calendarEventService.delete(eventId);
        }else if (appUserOptional.get().getId() == id) {
            AppUser appUser = appUserOptional.get();
            CalendarEvent calendarEvent = optionalCalendarEvent.get();
            appUser.getCalendarEvents().remove(calendarEvent);
            calendarEventService.delete(eventId);
        }
        return "user/profile";
    }

    @GetMapping("/modify/{id}/{eventId}")
    public String update(Model model, @PathVariable(name = "id") Long id, @PathVariable(name = "eventId") Long eventId){
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        Optional<CalendarEvent> optionalCalendarEvent = calendarEventService.findById(eventId);
        if (appUserOptional.get().isAdmin()) {
            Optional<AppUser> optionalAppUser = appUserService.findUserById(id);
            if (optionalAppUser.isPresent()) {
                if (optionalCalendarEvent.isPresent()) {
                    CalendarEvent calendarEvent = optionalCalendarEvent.get();
                    UpdateCalendarEventDto dto = UpdateCalendarEventDto.prepareToUpdate(calendarEvent);
                    AppUser appUser = optionalAppUser.get();
                    appUser.getCalendarEvents().remove(calendarEvent);
                    model.addAttribute("modify_event", dto);
                    return "event/modify";
                }
            }
            return "event/list";
        }else if (appUserOptional.get().getId() == id) {
            CalendarEvent calendarEvent = optionalCalendarEvent.get();
            AppUser appUser = appUserOptional.get();
            UpdateCalendarEventDto dto = UpdateCalendarEventDto.prepareToUpdate(calendarEvent);
            appUser.getCalendarEvents().remove(calendarEvent);
            model.addAttribute("modify_event", dto);
            return "event/modify";
        }
        return "event/list";
    }

    @PostMapping("/modify")
    public String update(UpdateCalendarEventDto dto){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()){
            calendarEventService.update(dto);
            return "redirect:/user/list";
        }
        return "redirect:/login";
    }

    @GetMapping("/add")
    public String add(Model model) {
        AddCalendarEventDto dto = new AddCalendarEventDto();
        model.addAttribute("added_event", dto);
        return "event/add_form";
    }

    @PostMapping("/add")
    public String add(Model model, AddCalendarEventDto dto) {
        if (dto.getStartDate() == null || dto.getPlace().isEmpty() ||
                dto.getName().isEmpty()) {
            model.addAttribute("added_event", dto);
            model.addAttribute("error_message", "Some field is empty.");
            return "event/add_form";
        }
        calendarEventService.add(dto);
        return "redirect:/view/user/profile";
    }

    @PostMapping("/addGuest/{id}/{eventId}")
    public String addGuest(@PathVariable(name = "id")Long id,@PathVariable(name = "eventId")Long eventId){
        Optional<AppUser> optionalAppUser = appUserService.findUserById(id);
        if (optionalAppUser.isPresent()){
            Optional<CalendarEvent> optionalCalendarEvent = calendarEventService.findById(eventId);
            if (optionalCalendarEvent.isPresent()){
                AppUser appUser = optionalAppUser.get();
                CalendarEvent calendarEvent = optionalCalendarEvent.get();
                calendarEvent.getGuests().add(appUser);
                calendarEventService.updateGuestList(calendarEvent);
                mailingService.sendEmail(appUser,"You are invited to event called " + calendarEvent.getName()
                        + " by " + calendarEvent.getOwner().getUsername());
                return "event/info";
            }
            return "user/friends";
        }
        return "event/info";
    }

    @GetMapping("/all")
    public String getAll(Model model){
        List<CalendarEvent> list = calendarEventService.getAll();
        model.addAttribute("event_list",list);
        return "event/list";
    }

    @GetMapping("/guestList/{id}")
    public String getGuestList(Model model,@PathVariable(name = "id")Long id){
        Optional<CalendarEvent> optionalCalendarEvent = calendarEventService.findById(id);
        if (optionalCalendarEvent.isPresent()){
            CalendarEvent calendarEvent = optionalCalendarEvent.get();
            List<AppUser> guestList = calendarEvent.getGuests();
            model.addAttribute("guestList",guestList);
        }
        return "event/guestList";
    }

    @GetMapping("/info/{id}")
    public String getInfo(Model model,@PathVariable(name = "id")Long id){
        Optional<CalendarEvent> optionalCalendarEvent = calendarEventService.findById(id);
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalCalendarEvent.isPresent()){
            if (optionalAppUser.isPresent()) {
                AppUser appUser = optionalAppUser.get();
                CalendarEvent calendarEvent = optionalCalendarEvent.get();
                model.addAttribute("event", calendarEvent);
                model.addAttribute("user", appUser);
                return "event/info";
            }
        }
        return "user/profile";
    }

}
