package pl.sda.lastProject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.lastProject.model.CalendarEvent;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCalendarEventDto {

    private Long id;

    private String name;
    private String place;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Double price;
    private boolean isPrivate;
    private String description;

    public static UpdateCalendarEventDto prepareToUpdate(CalendarEvent calendarEvent){
        UpdateCalendarEventDto dto = new UpdateCalendarEventDto();

        dto.setName(calendarEvent.getName());
        dto.setDescription(calendarEvent.getDescription());
        dto.setEndDate(calendarEvent.getEndDate());
        dto.setStartDate(calendarEvent.getStartDate());
        dto.setPlace(calendarEvent.getPlace());
        dto.setPrivate(calendarEvent.isPrivate());
        dto.setPrice(calendarEvent.getPrice());

        return dto;
    }
}
