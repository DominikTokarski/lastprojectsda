package pl.sda.lastProject.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class Schedule {

    @Qualifier("threadPoolTaskScheduler")
    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    private CronTrigger cronTrigger;

    @Autowired
    private PeriodicTrigger periodicTrigger;


    @PostConstruct
    public void scheduleRunnableWithCronTrigger() {
        taskScheduler.schedule(new MailNotify(),periodicTrigger);

    }

}
