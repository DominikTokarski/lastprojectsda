package pl.sda.lastProject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.lastProject.model.CalendarDay;
import pl.sda.lastProject.model.CalendarEvent;
import pl.sda.lastProject.repository.CalendarDayRepository;

import java.time.LocalDateTime;
import java.time.Year;
import java.util.Optional;

@Service
@Transactional
public class CalendarDayService {

    @Autowired
    private CalendarDayRepository calendarDayRepository;

    public Optional<CalendarDay> add(CalendarEvent calendarEvent) {
        LocalDateTime startDate = calendarEvent.getStartDate();
        Optional<CalendarDay> optionalCalendarDay = calendarDayRepository
                .findByDayOfMonthAndMonthAndYear(startDate.getDayOfMonth(),startDate.getMonth(),startDate.getYear());
        if (optionalCalendarDay.isPresent()){
            CalendarDay calendarDay = optionalCalendarDay.get();
            Optional<CalendarEvent> optionalCalendarEvent = Optional.of(calendarEvent);
            if (optionalCalendarEvent.isPresent()){
                calendarDay.getCalendarEvents().add(calendarEvent);
                calendarDayRepository.save(calendarDay);
                return Optional.of(calendarDay);
            }
            return Optional.empty();
        }
        CalendarDay calendarDay = new CalendarDay();
        calendarDay.setDayOfMonth(startDate.getDayOfMonth());
        calendarDay.setDayOfWeek(startDate.getDayOfWeek());
        calendarDay.setMonth(startDate.getMonth());
        calendarDay.setYear(startDate.getYear());
        calendarDay.getCalendarEvents().add(calendarEvent);
        calendarDayRepository.save(calendarDay);
        return Optional.of(calendarDay);
    }
}
