package pl.sda.lastProject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.lastProject.model.AppUser;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserDto {

    private Long id;
    private String password;
    private String name;
    private String surname;
    private Date birthDate;
    private String address;


    public static UpdateUserDto prepareToUpdate(AppUser appUser){
        UpdateUserDto dto = new UpdateUserDto();

        dto.setAddress(appUser.getAddress());
        dto.setBirthDate(appUser.getBirthDate());
        dto.setId(appUser.getId());
        dto.setName(appUser.getName());
        dto.setPassword(appUser.getPassword());
        dto.setSurname(appUser.getSurname());
        return dto;
    }
}
