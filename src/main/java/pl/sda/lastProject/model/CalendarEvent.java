package pl.sda.lastProject.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class CalendarEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String place;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Double price;
    private boolean isPrivate;
    private String description;

    @OneToMany(fetch = FetchType.EAGER)
    private List<AppUser> guests = new ArrayList<>();

    @ElementCollection(targetClass=String.class)
    private List<String> opinions = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    private AppUser owner;

}
