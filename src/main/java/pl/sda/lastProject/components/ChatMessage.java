package pl.sda.lastProject.components;




import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;



@Component
@Setter
@Getter
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }

}
