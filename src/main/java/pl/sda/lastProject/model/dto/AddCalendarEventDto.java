package pl.sda.lastProject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.lastProject.model.AppUser;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddCalendarEventDto {

    @NotEmpty
    private String name;

    @NotEmpty
    private String place;

    @NotEmpty
    private String startDate;

    private String endDate;
    private Double price;
    private boolean isPrivate;
    private String description;
    private AppUser owner;
}
