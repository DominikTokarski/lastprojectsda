package pl.sda.lastProject.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.lastProject.model.CalendarEvent;
import pl.sda.lastProject.repository.CalendarEventRepository;
import pl.sda.lastProject.service.MailingService;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class MailNotify implements Runnable {

    @Autowired
    private CalendarEventRepository calendarEventRepository;

    @Autowired
    MailingService mailingService;

    @Override
    public void run() {
        LocalDateTime time = LocalDateTime.now();
        List<CalendarEvent> calendarEvents = calendarEventRepository.findAll();
        if (calendarEvents.isEmpty()){
            return;
        }
        for (CalendarEvent event: calendarEvents){
            LocalDateTime dateTime = event.getStartDate();
            if (dateTime.getYear() == time.getYear() && dateTime.getMonth() == time.getMonth()
                    && dateTime.getDayOfMonth() == time.getDayOfMonth() - 1){
                mailingService.sendEmail(event.getOwner(),"You have event called "
                        + event.getName() + " tomorrow");
            }
        }
    }
}
