package pl.sda.lastProject.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import pl.sda.lastProject.model.AppUser;


import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailingService {

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(AppUser appUser, String content){
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setTo(appUser.getEmail());
            helper.setSubject("Register Account in Ordering app");
            helper.setText(content);
            helper.setFrom(from);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
