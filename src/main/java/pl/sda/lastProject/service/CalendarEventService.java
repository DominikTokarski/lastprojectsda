package pl.sda.lastProject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.lastProject.model.AppUser;
import pl.sda.lastProject.model.CalendarEvent;
import pl.sda.lastProject.model.dto.AddCalendarEventDto;
import pl.sda.lastProject.model.dto.UpdateCalendarEventDto;
import pl.sda.lastProject.repository.AppUserRepository;
import pl.sda.lastProject.repository.CalendarEventRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CalendarEventService {

    @Autowired
    private LoginService loginService;

    @Autowired
    private CalendarEventRepository calendarEventRepository;

    @Autowired
    private CalendarDayService calendarDayService;

    @Autowired
    private AppUserRepository appUserRepository;

    public Optional<CalendarEvent> findById(Long id) {
        return calendarEventRepository.findById(id);
    }

    public boolean delete(Long id) {
        if (calendarEventRepository.existsById(id)) {
            calendarEventRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Optional<CalendarEvent> update(UpdateCalendarEventDto dto) {
        Optional<CalendarEvent> optionalCalendarEvent = calendarEventRepository.findById(dto.getId());
        if (optionalCalendarEvent.isPresent()){
            CalendarEvent calendarEvent = optionalCalendarEvent.get();

            calendarEvent.setDescription(dto.getDescription());
            calendarEvent.setEndDate(dto.getEndDate());
            calendarEvent.setName(dto.getName());
            calendarEvent.setPlace(dto.getPlace());
            calendarEvent.setPrice(dto.getPrice());
            calendarEvent.setStartDate(dto.getStartDate());
            calendarEvent.setPrivate(dto.isPrivate());
            calendarEventRepository.save(calendarEvent);
            return Optional.of(calendarEvent);
        }
        return Optional.empty();
    }

    public Optional<CalendarEvent> add(AddCalendarEventDto dto) {
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()){
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            AppUser appUser = optionalAppUser.get();
            CalendarEvent calendarEvent = new CalendarEvent();
            calendarEvent.setPrivate(dto.isPrivate());
            calendarEvent.setStartDate(LocalDateTime.parse(dto.getStartDate(),dateTimeFormatter));
            calendarEvent.setPrice(dto.getPrice());
            calendarEvent.setPlace(dto.getPlace());
            calendarEvent.setOwner(appUser);
            calendarEvent.setName(dto.getName());
            calendarEvent.setDescription(dto.getDescription());
            calendarEvent.setEndDate(LocalDateTime.parse(dto.getStartDate(),dateTimeFormatter));
            calendarEvent = calendarEventRepository.save(calendarEvent);
            appUser.getCalendarEvents().add(calendarEvent);
            calendarDayService.add(calendarEvent);
            appUserRepository.save(appUser);
            return Optional.of(calendarEvent);
        }
        return Optional.empty();
    }

    public Optional<CalendarEvent> updateGuestList(CalendarEvent calendarEvent) {
        Optional<CalendarEvent> optionalCalendarEvent = Optional.of(calendarEvent);
        if (optionalCalendarEvent.isPresent()){
            calendarEventRepository.save(calendarEvent);
            return optionalCalendarEvent;
        }
        return Optional.empty();
    }

    public List<CalendarEvent> getAll() {
        return calendarEventRepository.findAll();
    }
}
