package pl.sda.lastProject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.lastProject.model.AppUser;
import pl.sda.lastProject.model.dto.CreateUserDto;
import pl.sda.lastProject.model.dto.UpdateUserDto;
import pl.sda.lastProject.repository.AppUserRepository;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private MailingService mailingService;



    public Optional<AppUser> addUser(CreateUserDto user) {
        if (!appUserRepository.findByUsername(user.getUsername()).isPresent()) {

            AppUser createdUser = AppUser.createFromDto(user);
            createdUser.setPassword(encoder.encode(user.getPassword()));
            createdUser = appUserRepository.save(createdUser);
            mailingService.sendEmail(createdUser,"Thanks for register");
            return Optional.of(createdUser);
        }

        return Optional.empty();
    }



    public Optional<AppUser> findByUsername(String email) {
        return appUserRepository.findByUsername(email);
    }

    public List<AppUser> getAll() {
        return appUserRepository.findAll();
    }

    public Optional<AppUser> findUserById(Long id) {
        return appUserRepository.findById(id);
    }

    public boolean delete(Long id) {
        if (appUserRepository.existsById(id)) {
            appUserRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Optional<AppUser> update(UpdateUserDto dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(dto.getId());
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();
            appUser.setName(dto.getName());
            appUser.setPassword(dto.getPassword());
            appUser.setSurname(dto.getSurname());
            appUser.setAddress(dto.getAddress());
            appUser.setBirthDate(dto.getBirthDate());
            appUserRepository.save(appUser);
            return Optional.of(appUser);
        }
        return  Optional.empty();
    }

    public Optional<AppUser> updateFriends(AppUser appUser) {
        Optional<AppUser> optionalAppUser = Optional.of(appUser);
        if (optionalAppUser.isPresent()) {
            appUserRepository.save(appUser);
            return optionalAppUser;
        }
        return Optional.empty();
    }


}
